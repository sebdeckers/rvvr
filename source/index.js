/*
  1. search all html/svg, js, and css files
  1. for every file found, process it
  1. read the file data
  1. parse data into ast
  1. traverse ast to find imports
  1. resolve each imported path
  1. read the imported files
  1. hash the imported files
  1. cache the hashes
  1. append `?v=${hash}` to the imported paths in ast
  1. yield the modified ast
  1. (cli) serialise the modified ast to output file
*/

const readdir = require('readdir-enhanced')

module.exports = async function * rvvr ({
  input,
  output,
  webroot = input,
  force = false,
  position = 'query', // or 'name'
  encoding = 'hex', // or 'emoji'
  length = encoding === 'emoji' ? 1 : 4
} = {}) {
  if (input === undefined) {
    throw new Error('input is undefined')
  }
  // if (output === undefined) {
  //   throw new Error('output is undefined')
  // }
  for await (const path of readdir.stream(input, { deep: true })) {
    yield path
  }
}
