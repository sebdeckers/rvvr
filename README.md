# rvvr 🎰

Inject a content hash to imported subresource paths of HTML, SVG, JS, or CSS files. Allows long-term, immutable HTTP caching.

```shell
$ rvvr --input './src/**.{js,html,css,svg}' --output ./dist
```

## Examples

| Format | Encoding | Length | Example         |
|--------|----------|--------|-----------------|
| query  | hex      | 4      | file.png?v=43f7 |
| name   | hex      | 4      | file.a83e.svg   |
| query  | emoji    | 1      | file.css?v=🍄   |
| name   | emoji    | 1      | file.🦄.js      |

## Supported Syntax

### HTML & SVG

All `href` and `src` attributes.

```html
<link rel='stylesheet' href='./design.css'>
<script src='./code.js'></script>
<img src='./image.jpg'>
```

### CSS

All `url()` values.

```css
@import './module.css';
@font-face { src: url('./font.woff'); }
h1 { background: url('./pattern.svg'); }
```

### JavaScript

All `import` or named `export` statements.

```js
import './module.js'
export * from './package.js'
```

## CLI

### `--input ./path/to/files`

- Alias: `-i`
- Required: Yes
- Multiple: Yes

Path to a file, or wildcard ([glob pattern](https://github.com/sindresorhus/globby#globbing-patterns)) to multiple files.

### `--output ./path/to/directory`

- Alias: `-o`
- Required: Yes

Path to a directory to write all files. Directory structure is preserved.

### `--webroot ./path/to/webroot`

- Alias: `-w`
- Default: Same as `--input`

Directory path from which to resolve absolute resource paths and URLs.

### `--force`, `--no-force`

- Alias: `-f`
- Default: `false`

Overwrite existing files.

### `--position`

- Alias: `-p`
- Values: `query`, `name`
- Default: `query`

Either modify the file name of each subresource, or append a query string containing the content hash.

Both methods are widely used for many years and compatible with all browsers, servers, and proxies.

The `query` approach does not rename files, which may be convenient by retaining consistent file names across versions.

The `name` approach allows for simultaneous hosting of multiple versions. Each version of the same source file will have its own unique filename.

### `--encoding`

- Alias: `-e`
- Values: `hex`, `emoji`
- Default: `hex`

Character encoding to use for the hash. Hexadecimal (0-9, a-f) or [base-emoji-512](https://www.npmjs.com/package/base-emoji-512).

### `--length`

- Alias: `-l`
- Default: `4` (hex) or `1` (emoji)

Number of characters to use in the hash.

## API

```js
const rvvr = require('rvvr')

for await (const resource of rvvr(input, options)) {
  // resource:
  // { from, to, hash, data }
}
```

### `rvvr(input, options)`

Returns an async generator that emits processed resources.

#### `input`

Type: String or Array

One or more file paths to process. Globs are supported.

## See Also

- [Unbundle](https://www.npmjs.com/package/unbundle) - Trace JS imports and place all subresources in their own directory. Use this before rvvr.
- [@commonshost/manifest](https://www.npmjs.com/package/@commonshost/manifest) - Trace all website subresources to build HTTP/2 Server Push instructions for web servers and CDNs.
