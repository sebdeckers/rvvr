const test = require('blue-tape')
const { join } = require('path')
const rvvr = require('..')

test('Rewrite dependencies', async (t) => {
  const input = join(__dirname, 'fixtures')
  for await (const file of rvvr({ input })) {
    t.ok(file, file)
  }
})
